package ulid

import "errors"

var ErrCannotGenerateUlid = errors.New("can't generate int of ulid")
