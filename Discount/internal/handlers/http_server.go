package handlers

import (
	"discount/internal/configs"
	"discount/internal/services"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

type HttpRouter struct {
	Service     services.IService
	EnvConfigs  configs.EnvConfigs
	EventRouter EventRouter
}

type IHttpRouter interface {
	Create(ctx *gin.Context)
}

var upgrader = websocket.Upgrader{}

func SetupHttpRouter(service services.IService, envConfigs configs.EnvConfigs) *gin.Engine {
	engine := gin.Default()

	// engine.Use(middlewares.SkipInternalErrorsInProductionMode())
	// engine.Use(cors.New(cors.Config{
	// 	AllowOrigins: []string{"*"},
	// 	AllowHeaders: []string{"*"},
	// 	AllowMethods: []string{"OPTIONS", "POST", "GET"},
	// }))

	linkCtrl := newHttpRouter(service, envConfigs)
	engine.POST("/discounts/apply", linkCtrl.ApplyDiscount)
	engine.GET("/", linkCtrl.GetTransactions)

	return engine
}

func newHttpRouter(service services.IService, envConfig configs.EnvConfigs) HttpRouter {
	ctrl := HttpRouter{
		Service:    service,
		EnvConfigs: envConfig,
	}

	return ctrl
}

func (c HttpRouter) Response(ctx *gin.Context, statusCode int, responseBody interface{}) {

	if statusCode == http.StatusInternalServerError && c.EnvConfigs.EnvironmentMode != "Development" {
		ctx.JSON(statusCode, gin.H{"error": "internal error"})
	} else {
		ctx.JSON(statusCode, responseBody)
	}
}
