package mappers

import (
	"discount/internal/handlers/dtos"
	"discount/internal/translator"
	"discount/internal/validation"
)

func CustomValidationErrorToValidationErrorDTO(customValidationError validation.CustomValidationError) *dtos.ValidationErrorDTO {
	var validationErrorDTO dtos.ValidationErrorDTO

	validationErrorDTO.Detail = translator.Translate(customValidationError.Message, "en")
	validationErrorDTO.DetailLocale = translator.Translate(customValidationError.Message, "fa")
	validationErrorDTO.Fields = CustomFieldErrorsToFieldErrorDTOs(customValidationError.Fields)
	return &validationErrorDTO
}

func CustomFieldErrorsToFieldErrorDTOs(customFieldErrors []validation.CustomFieldError) []dtos.FieldErrorDTO {
	var fieldErrorDTOs []dtos.FieldErrorDTO

	for _, v := range customFieldErrors {
		var fieldErrorDTO dtos.FieldErrorDTO
		fieldErrorDTO.ID = v.Name
		fieldErrorDTO.Detail = translator.Translate(v.Message, "en")
		fieldErrorDTO.DetailLocale = translator.Translate(v.Message, "fa")

		fieldErrorDTOs = append(fieldErrorDTOs, fieldErrorDTO)
	}

	return fieldErrorDTOs
}
