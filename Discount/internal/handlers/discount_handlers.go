package handlers

import (
	"discount/internal/handlers/dtos"
	"discount/internal/models"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

func (h HttpRouter) ApplyDiscount(ctx *gin.Context) {
	var req dtos.ApplyDisountDTO
	if err := ctx.BindJSON(&req); err != nil {
		h.Response(ctx, http.StatusInternalServerError, err.Error())
	}

	err := h.Service.ApplyDiscount(req)
	if err != nil {
		var validationError *dtos.ValidationErrorDTO
		var notFound *models.ErrorNotFound
		if errors.As(err, &validationError) {
			h.Response(ctx, http.StatusBadRequest, validationError)
			return
		}
		if errors.As(notFound, &validationError) {
			h.Response(ctx, http.StatusNotFound, notFound)
			return
		}
		if errors.Is(err, models.DiscountFullFillError) {
			h.Response(ctx, http.StatusNotFound, err.Error())
		}
		if errors.Is(err, models.UserUsedDiscount) {
			h.Response(ctx, http.StatusBadRequest, validationError)
		}

		h.Response(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	h.Response(ctx, http.StatusOK, nil)
}

func (h HttpRouter) GetTransactions(ctx *gin.Context) {
	//upgrade get request to websocket protocol
	ws, err := upgrader.Upgrade(ctx.Writer, ctx.Request, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer ws.Close()

	for approvedTransaction := range h.EventRouter.ApprovedTransactionDtoChannel {

		message, err := json.Marshal(approvedTransaction)
		if err != nil {
			break
		}

		err = ws.WriteMessage(websocket.BinaryMessage, message)
		if err != nil {
			break
		}
	}
}
