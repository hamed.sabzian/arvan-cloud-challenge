package dtos

import (
	"encoding/json"
)

type MetaData struct {
	UserID         string `header:"X-User-Id" validate:"required"`
	DeviceUniqueID string `header:"device_unique_id"`
	DeviceIMEI     string `header:"device_imei"`
	DeviceMac      string `header:"device_mac"`
	XForwadedFor   string `header:"host_name"`
	Referer        string `header:"referer"`
	UserAgent      string `header:"user_agent"`
	Country        string `header:"country"`
}

type Meta struct {
	CurrentPage int `json:"current_page"`
	PerPage     int `json:"per_page"`
	Total       int `json:"total"`
	From        int `json:"from"`
	To          int `json:"to"`
}

type ErrNotFound struct {
	Detail       string `json:"detail"`
	DetailLocale string `json:"detail_locale"`
}

func (e *ErrNotFound) Error() string {
	return e.Detail + ": " + e.DetailLocale //TODO:
}

type ValidationErrorDTO struct {
	Detail       string          `json:"detail"`
	DetailLocale string          `json:"detail_locale"`
	Fields       []FieldErrorDTO `json:"fields"`
}

func (e *ValidationErrorDTO) Error() string {
	return e.Detail + ": " + e.DetailLocale //TODO:
}

func (e *ValidationErrorDTO) ToJSON() string {
	ret, _ := json.Marshal(e)
	return string(ret)
}

type FieldErrorDTO struct {
	ID           string `json:"id"`
	Detail       string `json:"detail"`
	DetailLocale string `json:"detail_local"`
}

func (e *FieldErrorDTO) Error() string {
	return e.ID + ": " + e.Detail + ": " + e.DetailLocale //TODO:

}

func (e *FieldErrorDTO) ToJSON() string {
	ret, _ := json.Marshal(e)
	return string(ret)
}
