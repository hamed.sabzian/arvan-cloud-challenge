package dtos

type ApplyDisountDTO struct {
	Code   string `json:"code"`
	Mobile string `json:"mobile"`
}
