package handlers

import (
	"context"
	"discount/internal/brokers"
	"discount/internal/brokers/brokersdtos"
	"discount/internal/services"
	"fmt"
)

type EventRouter struct {
	Service                       services.IService
	Broker                        brokers.Broker
	SuccessTransactionDtoChannel  chan brokersdtos.SuccessTransactionDTO
	FailedTransactionDtoChannel   chan brokersdtos.FailedTransactionDTO
	ApprovedTransactionDtoChannel chan brokersdtos.ApprovedTransactionDTO
}

func NewEventHandler(service services.IService, broker brokers.Broker) EventRouter {
	return EventRouter{
		Service:                       service,
		Broker:                        broker,
		SuccessTransactionDtoChannel:  make(chan brokersdtos.SuccessTransactionDTO),
		FailedTransactionDtoChannel:   make(chan brokersdtos.FailedTransactionDTO),
		ApprovedTransactionDtoChannel: make(chan brokersdtos.ApprovedTransactionDTO),
	}
}

func (e *EventRouter) StartSuccessTransactionConsumer() error {
	go e.SuccessTransactionReader()
	go e.Broker.SuccessTransactionReader(context.Background(), e.SuccessTransactionDtoChannel)

	return nil
}

func (e *EventRouter) StartFailedTransactionConsumer() error {
	go e.FailedTransactionReader()
	go e.Broker.FailedTransactionReader(context.Background(), e.FailedTransactionDtoChannel)

	return nil
}

func (e *EventRouter) StartApprovedTransactionConsumer() error {
	go e.Broker.ApprovedTransactionReader(context.Background(), e.ApprovedTransactionDtoChannel)

	return nil
}

//nolint: forbidigo
func (e EventRouter) SuccessTransactionReader() {
	for successTransactionDtoChannel := range e.SuccessTransactionDtoChannel {
		err := e.Service.SuccessTransaction(successTransactionDtoChannel)
		if err != nil {
			fmt.Println(err)
		}
	}
}

func (e EventRouter) FailedTransactionReader() {
	for failedTransactionDtoChannel := range e.FailedTransactionDtoChannel {
		err := e.Service.FailedTransaction(failedTransactionDtoChannel)
		if err != nil {
			fmt.Println(err)
		}
	}
}
