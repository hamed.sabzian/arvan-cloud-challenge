package validation

import (
	"encoding/json"
	"errors"

	"github.com/go-playground/validator/v10"
)

var UnknownErrorValidationError = errors.New("Unknown error validation error")

type CustomValidationError struct {
	Message string
	Fields  []CustomFieldError
}

func (e *CustomValidationError) Error() string {
	ret, _ := json.Marshal(e)
	return string(ret)
}

type CustomFieldError struct {
	Name    string
	Message string
}

func (e *CustomFieldError) Error() string {
	ret, _ := json.Marshal(e)
	return string(ret)
}

func initValidationError(error validator.FieldError) (*CustomFieldError, error) {
	var fieldError CustomFieldError

	fieldError.Name = error.Namespace()

	message, err := getFieldErrorMessage(error.Tag())
	if err != nil {
		return nil, err
	}
	fieldError.Message = message

	return &fieldError, nil
}

func getFieldErrorMessage(tagError string) (string, error) {
	//TODO: add error for length of database fields
	switch tagError {
	case "required":
		return "this.field.is.required", nil
	default:
		return "", errors.New("Unknown error validation error")
	}
}
