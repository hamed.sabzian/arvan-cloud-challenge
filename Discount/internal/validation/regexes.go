package validation

import "regexp"

const (
	MobileString = `^(\+98|0|0098)(\(|)9{1}\d{2}(\)|(-|))\d{3}(-|)\d{4}$`
)

var MobileRegex = regexp.MustCompile(MobileString)
