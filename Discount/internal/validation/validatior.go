package validation

import (
	"errors"

	"github.com/go-playground/validator/v10"
)

type ValidationEngine struct {
	Validator *validator.Validate
}

// isMobilePhone is the validation function for validating if the current field's value is a valid phone number.
func isMobilePhone(fl validator.FieldLevel) bool {
	return MobileRegex.MatchString(fl.Field().String())
}

func NewValidator() *ValidationEngine {

	validator := validator.New()

	err := validator.RegisterValidation("mobile", isMobilePhone)
	if err != nil {
		panic(err)
	}

	return &ValidationEngine{Validator: validator}
}

func (v *ValidationEngine) Validate(toValidate interface{}) error {

	err := v.Validator.Struct(toValidate)
	if err != nil {
		var valErrs validator.ValidationErrors
		if errors.As(err, &valErrs) {
			var validationError CustomValidationError
			validationError.Message = "validation.error"
			for _, e := range valErrs {
				customFieldError, err := initValidationError(e)
				if err != nil {
					return err
				}
				validationError.Fields = append(validationError.Fields, *customFieldError)
			}
			return &validationError
		}
	}

	return nil
}
