package tests

import (
	"discount/internal/brokers"
	"discount/internal/configs"
	"discount/internal/handlers"
	"discount/internal/repositories"
	"discount/internal/services"
	"discount/internal/validation"
	"fmt"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
)

var (
	sqlConnection             *sqlx.DB
	router                    *gin.Engine
	service                   services.IService
	repository                repositories.SqlxRepository
	sampleCampaignWalletID    int64
	sampleDestinationWalletID int64
)

func TestMain(m *testing.M) {

	// err := godotenv.Load("../../.env")
	// if err != nil {
	// 	log.Fatal("Error loading .env file")
	// }

	fmt.Print("running tests")

	envConfigs := configs.LoadEnvConfigs()

	sqlxConnection := repositories.InitSqlx(envConfigs)

	repository := repositories.NewSqlxRepository(sqlxConnection)

	validator := validation.NewValidator()

	rabbitmq := brokers.NewRabbitmq(envConfigs)

	service := services.NewService(repository, rabbitmq, *validator, envConfigs)

	eventHandlerEngine := handlers.NewEventHandler(service, rabbitmq)

	err := eventHandlerEngine.StartSuccessTransactionConsumer()
	if err != nil {
		panic(err)
	}
	err = eventHandlerEngine.StartFailedTransactionConsumer()
	if err != nil {
		panic(err)
	}

	router = handlers.SetupHttpRouter(service, envConfigs)

	code := m.Run()

	os.Exit(code)
}

func deleteAllData() {
	sqlConnection.Exec("DELETE FROM discounts")
	sqlConnection.Exec("DELETE FROM discount_transactions")
}
