package tests

import (
	"bytes"
	"discount/internal/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestApplayDiscount(t *testing.T) {
	defer deleteAllData()

	requestBody := map[string]interface{}{"code": "nowrouz", "mobile": "09212161273"}
	jsonRequestBody, err := json.Marshal(requestBody)
	if err != nil {
		panic(err)
	}

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/discounts/apply", bytes.NewBuffer(jsonRequestBody))
	req.Header.Set("Content-Type", "application/json")

	router.ServeHTTP(w, req)

	response := w.Result()

	defer response.Body.Close()

	responseBody, readErr := ioutil.ReadAll(response.Body)
	if readErr != nil {
		panic(err)
	}
	fmt.Println("Test response body ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + string(responseBody))

	time.Sleep(15 * time.Second)
	require.Equal(t, http.StatusOK, response.StatusCode)
}

func TestApplayDiscountWhenReturnNotFoundError(t *testing.T) {
	defer deleteAllData()

	requestBody := map[string]interface{}{"code": "invalid code", "mobile": "09212161273"}
	jsonRequestBody, err := json.Marshal(requestBody)
	if err != nil {
		panic(err)
	}

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/discounts/apply", bytes.NewBuffer(jsonRequestBody))
	req.Header.Set("Content-Type", "application/json")

	router.ServeHTTP(w, req)

	response := w.Result()

	defer response.Body.Close()

	responseBody, readErr := ioutil.ReadAll(response.Body)
	if readErr != nil {
		panic(err)
	}
	fmt.Println("Test response body ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + string(responseBody))

	notFoundError := models.ErrorNotFound{}
	jsonErr := json.Unmarshal(responseBody, &notFoundError)
	if jsonErr != nil {
		panic(jsonErr)
	}
	time.Sleep(15 * time.Second)
	require.Equal(t, http.StatusNotFound, response.StatusCode)
}
