package configs

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type EnvConfigs struct {
	HTTPIP              string
	HTTPPort            string
	DatabaseDriver      string
	DatabaseName        string
	DatabaseHost        string
	DatabasePort        string
	DatabaseUser        string
	DatabasePass        string
	DatabaseSSLMode     string
	DatabaseMaxPageSize string

	RabbitMqHost                  string
	Exchange                      string
	ExchangeType                  string
	ReconnectDelay                string
	PendingTransactionRoutingKey  string
	SuccessTransactionQueue       string
	FailedTransactionQueue        string
	ApprovedTransactionQueue      string
	SuccessTransactionRoutingKey  string
	FailedTransactionRoutingKey   string
	ApprovedTransactionRoutingKey string

	// LogLevel              string
	// AppLogPath            string
	EnvironmentMode string

	SwitchNumber int
}

func LoadEnvConfigs() EnvConfigs {

	err := godotenv.Load("../../.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	HTTPIP := os.Getenv("HTTP_IP")
	HTTPPort := os.Getenv("HTTP_PORT")
	environmentMode := os.Getenv("ENVIRONMENT_MODE")

	DatabaseDriver := os.Getenv("DATABASE_DRIVER")
	DatabaseName := os.Getenv("DATABASE_NAME")
	DatabaseHost := os.Getenv("DATABASE_HOST")
	DatabasePort := os.Getenv("DATABASE_PORT")
	DatabaseUser := os.Getenv("DATABASE_USER")
	DatabasePass := os.Getenv("DATABASE_PASS")
	DatabaseSSLMode := os.Getenv("DATABASE_SSLMODE")
	DatabaseMaxPageSize := os.Getenv("DATABASE_MAX_PAGE_SIZE")

	rabbitmqHost := os.Getenv("RABBITMQ_HOST")
	reconnectDelay := os.Getenv("RECONNECT_DELAY")
	exchange := os.Getenv("EXCHANGE")
	exchangeType := os.Getenv("EXCHANGE_TYPE")
	pendingTransactionRoutingKey := os.Getenv("PENDING_TRANSACTION_ROUTING_KEY")
	successTransactionQueue := os.Getenv("SUCCESS_TRANSACTION_QUEUE")
	failedTransactionQueue := os.Getenv("FAILED_TRANSACTION_QUEUE")
	approvedTransactionQueue := os.Getenv("APPROVED_TRANSACTION_QUEUE")
	failedTransactionRoutingKey := os.Getenv("FAILED_TRANSACTION_ROUTING_KEY")
	successTransactionRoutingKey := os.Getenv("SUCCESS_TRANSACTION_ROUTING_KEY")
	approvedTransactionRoutingKey := os.Getenv("APPROVED_TRANSACTION_ROUTING_KEY")

	config := EnvConfigs{
		HTTPIP:   HTTPIP,
		HTTPPort: HTTPPort,

		RabbitMqHost:   rabbitmqHost,
		ReconnectDelay: reconnectDelay,
		Exchange:       exchange,
		ExchangeType:   exchangeType,

		DatabaseDriver:      DatabaseDriver,
		DatabaseName:        DatabaseName,
		DatabaseHost:        DatabaseHost,
		DatabasePort:        DatabasePort,
		DatabaseUser:        DatabaseUser,
		DatabasePass:        DatabasePass,
		DatabaseSSLMode:     DatabaseSSLMode,
		DatabaseMaxPageSize: DatabaseMaxPageSize,

		PendingTransactionRoutingKey:  pendingTransactionRoutingKey,
		SuccessTransactionQueue:       successTransactionQueue,
		FailedTransactionQueue:        failedTransactionQueue,
		ApprovedTransactionQueue:      approvedTransactionQueue,
		SuccessTransactionRoutingKey:  successTransactionRoutingKey,
		FailedTransactionRoutingKey:   failedTransactionRoutingKey,
		ApprovedTransactionRoutingKey: approvedTransactionRoutingKey,

		EnvironmentMode: environmentMode,
	}

	return config
}
