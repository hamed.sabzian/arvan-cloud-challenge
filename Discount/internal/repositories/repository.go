package repositories

import (
	"discount/internal/models"

	"github.com/jmoiron/sqlx"
)

type IRepository interface {
	GetDiscountByCode(tx *sqlx.Tx, code string) (*models.Discount, error)
	GetDiscountByID(tx *sqlx.Tx, id string) (*models.Discount, error)
	UpdateDiscount(tx *sqlx.Tx, discount models.Discount) (*models.Discount, error)
	AddTransaction(tx *sqlx.Tx, transaction models.Transaction) (*models.Transaction, error)
	UpdateDiscountTransaction(tx *sqlx.Tx, transaction models.Transaction) (*models.Transaction, error)
	GetPendingDiscountTransaction(tx *sqlx.Tx, mobile string) (*models.Transaction, error)

	Begin() *sqlx.Tx
}
