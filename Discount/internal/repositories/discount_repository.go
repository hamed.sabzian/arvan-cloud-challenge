package repositories

import (
	"database/sql"
	"discount/internal/models"
	"errors"
	"fmt"
	"strconv"
	"time"

	"git.wki.ir/idpay/pkg/id/ulid"
	"github.com/jmoiron/sqlx"
)

type TransactionDTO struct {
	ID         sql.NullString `db:"id"`
	TrackID    sql.NullInt64  `db:"track_id"`
	DiscountID sql.NullString `db:"discount_id"`
	Mobile     sql.NullString `db:"mobile"`
	Status     sql.NullString `db:"status"`
	CreatedAt  time.Time      `db:"created_at"`
	UpdatedAt  time.Time      `db:"updated_at"`
}

type DiscountDTO struct {
	ID            sql.NullString `db:"id"`
	Code          sql.NullString `db:"code"`
	Description   sql.NullString `db:"description"`
	MaxUsageCount sql.NullInt64  `db:"max_usage_count"`
	UsedCount     sql.NullInt64  `db:"used_count"` //TODO: check for better name
	CreatedAt     time.Time      `db:"created_at"`
	UpdatedAt     time.Time      `db:"updated_at"`
}

type DiscountWithRelations struct {
	DiscountDTO
	TransactionDTO
}

func (repo SqlxRepository) GetDiscountByCode(tx *sqlx.Tx, code string) (*models.Discount, error) {
	var discountWithRelations []DiscountWithRelations

	err := tx.Select(&discountWithRelations, "SELECT discounts.*, discount_transactions.* FROM discounts "+
		"LEFT JOIN discount_transactions on discounts.id = discount_transactions.discount_id WHERE discounts.code = $1", code)
	if err != nil {
		return nil, err
	}

	discount := models.Discount{
		ID:            discountWithRelations[0].DiscountDTO.ID.String,
		Code:          discountWithRelations[0].DiscountDTO.Code.String,
		Description:   discountWithRelations[0].DiscountDTO.Description.String,
		MaxUsageCount: discountWithRelations[0].DiscountDTO.MaxUsageCount.Int64,
		UsedCount:     discountWithRelations[0].DiscountDTO.UsedCount.Int64,
		CreatedAt:     discountWithRelations[0].DiscountDTO.CreatedAt,
		UpdatedAt:     discountWithRelations[0].DiscountDTO.UpdatedAt,
	}

	for _, discountWithRelation := range discountWithRelations {
		transaction := models.Transaction{
			ID:         discountWithRelation.TransactionDTO.ID.String,
			TrackID:    discountWithRelation.TransactionDTO.TrackID.Int64,
			DiscountID: discountWithRelation.TransactionDTO.DiscountID.String,
			Mobile:     discountWithRelation.TransactionDTO.Mobile.String,
			Status:     models.TransactionStatus(discountWithRelation.TransactionDTO.Status.String),
			CreatedAt:  discountWithRelation.TransactionDTO.CreatedAt,
			UpdatedAt:  discountWithRelation.TransactionDTO.UpdatedAt,
		}
		discount.Transactions = append(discount.Transactions, transaction)
	}

	return &discount, nil
}

func (repo SqlxRepository) GetDiscountByID(tx *sqlx.Tx, id string) (*models.Discount, error) {
	var discountDTO DiscountDTO

	err := tx.Get(&discountDTO, "SELECT * FROM discounts WHERE id = $1", id)
	if err != nil {
		return nil, err
	}

	discount := models.Discount{
		ID:            discountDTO.ID.String,
		Code:          discountDTO.Code.String,
		Description:   discountDTO.Description.String,
		MaxUsageCount: discountDTO.MaxUsageCount.Int64,
		UsedCount:     discountDTO.UsedCount.Int64,
		CreatedAt:     discountDTO.CreatedAt,
		UpdatedAt:     discountDTO.UpdatedAt,
	}

	return &discount, nil
}

func (repo SqlxRepository) UpdateDiscount(tx *sqlx.Tx, discount models.Discount) (*models.Discount, error) {
	discount.UpdatedAt = time.Now()

	_, err := tx.Exec("UPDATE discounts SET used_count=$1, updated_at=$2 WHERE id=$3 ", discount.UsedCount, discount.UpdatedAt, discount.ID)
	if err != nil {
		return nil, err
	}

	return &discount, nil
}

func (repo SqlxRepository) AddTransaction(tx *sqlx.Tx, transaction models.Transaction) (*models.Transaction, error) {
	transactionID := ulid.Generate()

	transactionTrackID, err := ulid.ToInt64(transactionID, strconv.Itoa(1))
	if err != nil {
		return nil, err
	}

	transaction.ID = transactionID
	transaction.TrackID = transactionTrackID

	transaction.CreatedAt = time.Now()
	transaction.UpdatedAt = time.Now()

	_, err = tx.Exec("INSERT INTO discount_transactions(discount_id, mobile, status, created_at)",
		transaction.DiscountID, transaction.Mobile, transaction.Status, transaction.CreatedAt)
	if err != nil {
		return nil, err
	}

	return &transaction, nil
}

func (repo SqlxRepository) UpdateDiscountTransaction(tx *sqlx.Tx, transaction models.Transaction) (*models.Transaction, error) {
	transaction.UpdatedAt = time.Now()

	_, err := tx.Exec("UPDATE discount_transactions SET status=$1, updated_at=$2 WHERE id=$3 ", transaction.Status, transaction.UpdatedAt)
	if err != nil {
		return nil, err
	}

	return &transaction, nil
}

func (repo SqlxRepository) GetPendingDiscountTransaction(tx *sqlx.Tx, mobile string) (*models.Transaction, error) {
	var transactionDTO TransactionDTO

	err := tx.Select(&transactionDTO, "SELECT * FROM discount_transactions WHERE id=$1 AND status=$2", mobile, models.PendingTransactionStatus)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, models.NewErrorNotFound("GetPendingDiscountTransaction", "")
		}

		return nil, fmt.Errorf("GetPendingDiscountTransaction >> selectOrder >>  execute sql: %w", err)
	}

	transaction := models.Transaction{
		ID:         transactionDTO.ID.String,
		TrackID:    transactionDTO.TrackID.Int64,
		DiscountID: transactionDTO.DiscountID.String,
		Mobile:     transactionDTO.Mobile.String,
		Status:     models.TransactionStatus(transactionDTO.Status.String),
		CreatedAt:  transactionDTO.CreatedAt,
		UpdatedAt:  transactionDTO.UpdatedAt,
	}

	return &transaction, nil
}
