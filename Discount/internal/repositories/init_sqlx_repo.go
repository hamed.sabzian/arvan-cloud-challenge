package repositories

import (
	"discount/internal/configs"
	"fmt"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" //nolint: revive
)

type SqlxRepository struct {
	DB  *sqlx.DB
	sql sq.StatementBuilderType
}

func InitSqlx(envConfig configs.EnvConfigs) *sqlx.DB {

	// ssl := "disable"

	dbinfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		envConfig.DatabaseHost, envConfig.DatabasePort, envConfig.DatabaseUser, envConfig.DatabasePass, envConfig.DatabaseName)

	db, err := sqlx.Open("postgres", dbinfo)
	if err != nil {
		panic(err)
	}

	if err = db.Ping(); err != nil {
		panic(err)
	}

	return db
}

func NewSqlxRepository(conn *sqlx.DB) SqlxRepository {
	dbRepository := SqlxRepository{
		DB:  conn,
		sql: sq.StatementBuilder.PlaceholderFormat(sq.Dollar),
	}

	return dbRepository
}
