package models

import (
	"errors"
	"time"
)

var (
	DiscountFullFillError = errors.New("Discount is full fill")
	UserUsedDiscount      = errors.New("User used discount")
)

type Discount struct {
	ID            string
	Code          string
	Description   string
	MaxUsageCount int64
	UsedCount     int64
	CreatedAt     time.Time
	UpdatedAt     time.Time
	Transactions  []Transaction
}

func (d *Discount) isValidCode() bool {
	return d.UsedCount < d.MaxUsageCount
}

func (d *Discount) AddNewTransaction(transaction Transaction) (*Transaction, error) {
	if d.isValidCode() == false {
		return nil, DiscountFullFillError
	}

	_, err := searchInList(d.Transactions, transaction.ID)
	if err == nil {
		return nil, UserUsedDiscount
	}

	d.Transactions = append(d.Transactions, transaction)
	d.UsedCount += 1

	return &transaction, nil
}

func (d *Discount) SetTransactionSuccessStatus(transactionID string) (*Transaction, error) {
	transaction, err := searchInList(d.Transactions, transactionID)
	if err != nil {
		return nil, err
	}
	transaction.Status = SuccessedTransactionStatus

	return transaction, nil
}

func (d *Discount) SetTransactionFailedStatus(transactionID string) (*Transaction, error) {
	transaction, err := searchInList(d.Transactions, transactionID)
	if err != nil {
		return nil, err
	}
	transaction.Status = FailedTransactionStatus
	d.MaxUsageCount -= 1

	return transaction, nil
}

func searchInList(transactions []Transaction, transactionID string) (*Transaction, error) {
	for _, transaction := range transactions {
		if transaction.ID == transactionID {
			return &transaction, nil
		}
	}

	return nil, errors.New("not found transaction")
}
