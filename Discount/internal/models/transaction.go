package models

import (
	"time"
)

type TransactionStatus string

const (
	PendingTransactionStatus   TransactionStatus = "PENDING"
	SuccessedTransactionStatus TransactionStatus = "SUCCESSED"
	FailedTransactionStatus    TransactionStatus = "FAILED"
)

type Transaction struct {
	ID         string
	TrackID    int64
	DiscountID string
	Mobile     string
	Status     TransactionStatus
	CreatedAt  time.Time
	UpdatedAt  time.Time
}
