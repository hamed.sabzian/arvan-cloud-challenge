package brokers

import (
	"discount/internal/configs"
	"fmt"
	"log"
	"strconv"

	amqp "github.com/rabbitmq/amqp091-go"
)

type RabbitMQ struct {
	Host                          string
	ReconnectDelay                int
	Exchange                      string
	ExchangeType                  string
	PendingTransactionRoutingKey  string
	SuccessTransactionRoutingKey  string
	FailedTransactionRoutingKey   string
	ApprovedTransactionRoutingKey string
	// WalletTransferQueue          string
	SuccessTransactionQueue  string
	FailedTransactionQueue   string
	ApprovedTransactionQueue string
	Channel                  *amqp.Channel
}

func (r *RabbitMQ) Connect(rabbitHost string) *amqp.Channel {
	if r.Channel != nil && !r.Channel.IsClosed() {
		return r.Channel
	}

	conn, err := amqp.Dial(rabbitHost)
	if err != nil {
		panic(err)
	}

	channel, err := conn.Channel()
	if err != nil {
		panic(err)
	}

	return channel
}

func NewRabbitmq(config configs.EnvConfigs) Broker {
	r := RabbitMQ{
		Host:                          config.RabbitMqHost,
		Exchange:                      config.Exchange,
		ExchangeType:                  config.ExchangeType,
		PendingTransactionRoutingKey:  config.PendingTransactionRoutingKey,
		SuccessTransactionRoutingKey:  config.SuccessTransactionRoutingKey,
		FailedTransactionRoutingKey:   config.FailedTransactionRoutingKey,
		SuccessTransactionQueue:       config.SuccessTransactionQueue,
		FailedTransactionQueue:        config.FailedTransactionQueue,
		ApprovedTransactionRoutingKey: config.ApprovedTransactionRoutingKey,
		ApprovedTransactionQueue:      config.ApprovedTransactionQueue,
	}

	var err error

	r.ReconnectDelay, err = strconv.Atoi("5")
	if err != nil {
		panic(err)
	}

	ch := r.Connect(config.RabbitMqHost)
	r.Channel = ch

	err = r.declareQueues()
	if err != nil {
		log.Fatal("Broket >> initialize >> NewRabbitmq >> Declare Queues", err)
	}

	return &r
}

func (r *RabbitMQ) declareQueues() error {
	err := r.Channel.ExchangeDeclare(
		r.Exchange,
		r.ExchangeType,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("declareQueues >> ExchangeDeclare >>  %w", err)
	}

	q, err := r.Channel.QueueDeclare(
		r.SuccessTransactionQueue,
		false,
		false,
		true,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("declareQueues >> QueueDeclare >> %w", err)
	}

	err = r.Channel.QueueBind(
		q.Name,
		r.SuccessTransactionRoutingKey,
		r.Exchange,
		false,
		nil)

	if err != nil {
		return fmt.Errorf("declareQueues >> QueueBind >> %w", err)
	}

	q, err = r.Channel.QueueDeclare(
		r.FailedTransactionQueue,
		false,
		false,
		true,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("declareQueues >> QueueDeclare >> %w", err)
	}

	err = r.Channel.QueueBind(
		q.Name,
		r.FailedTransactionRoutingKey,
		r.Exchange,
		false,
		nil)

	if err != nil {
		return fmt.Errorf("declareQueues >> QueueBind >> %w", err)
	}

	q, err = r.Channel.QueueDeclare(
		r.ApprovedTransactionQueue,
		false,
		false,
		true,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("declareQueues >> QueueDeclare >> %w", err)
	}

	err = r.Channel.QueueBind(
		q.Name,
		r.ApprovedTransactionRoutingKey,
		r.Exchange,
		false,
		nil)

	if err != nil {
		return fmt.Errorf("declareQueues >> QueueBind >> %w", err)
	}

	return nil
}
