package brokers

import (
	"context"
	"discount/internal/brokers/brokersdtos"
	"encoding/json"
	"fmt"
	"time"
)

func (r *RabbitMQ) FailedTransactionReader(ctx context.Context, failedTransactionDtoChannel chan brokersdtos.FailedTransactionDTO) error {
	var failedTransactionDto brokersdtos.FailedTransactionDTO

	for { // nolint
		msgs, err := r.Channel.Consume(
			r.FailedTransactionQueue, // queue
			"",                        // consumer
			false,                     // auto ack
			false,                     // exclusive
			false,                     // no local
			false,                     // no wait
			nil,                       // args
		)
		if err != nil {
			return fmt.Errorf("broker > rabbitmq > PaymentReader > consume > %w", err)
		}

		for msg := range msgs {
			unmarshalError := json.Unmarshal(msg.Body, &failedTransactionDto)
			if unmarshalError != nil {
				_ = msg.Nack(true, true)
			}

			failedTransactionDtoChannel <- failedTransactionDto

			msg.Ack(false)
		}

		if r.Channel.IsClosed() {
			time.Sleep(time.Duration(r.ReconnectDelay) * time.Second)
			r.Channel = r.Connect(r.Host)
		}
	}
}
