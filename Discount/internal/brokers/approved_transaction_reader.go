package brokers

import (
	"context"
	"discount/internal/brokers/brokersdtos"
	"encoding/json"
	"fmt"
	"time"
)

func (r *RabbitMQ) ApprovedTransactionReader(ctx context.Context, approvedTransactionDtoChannel chan brokersdtos.ApprovedTransactionDTO) error {
	var approvedTransactionDTO brokersdtos.ApprovedTransactionDTO

	for { // nolint
		msgs, err := r.Channel.Consume(
			r.ApprovedTransactionQueue, // queue
			"",                         // consumer
			false,                      // auto ack
			false,                      // exclusive
			false,                      // no local
			false,                      // no wait
			nil,                        // args
		)
		if err != nil {
			return fmt.Errorf("broker > rabbitmq > PaymentReader > consume > %w", err)
		}

		for msg := range msgs {
			unmarshalError := json.Unmarshal(msg.Body, &approvedTransactionDTO)
			if unmarshalError != nil {
				_ = msg.Nack(true, true)
			}

			approvedTransactionDtoChannel <- approvedTransactionDTO

			msg.Ack(false)
		}

		if r.Channel.IsClosed() {
			time.Sleep(time.Duration(r.ReconnectDelay) * time.Second)
			r.Channel = r.Connect(r.Host)
		}
	}
}
