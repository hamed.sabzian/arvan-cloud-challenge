package brokers

import (
	"context"
	"discount/internal/brokers/brokersdtos"
)

type Broker interface {
	PublishPendingTransaction(amount float64, mobile string) error
	PublishApprovedTransaction(amount float64, mobile string) error
	FailedTransactionReader(ctx context.Context, failedTransactionDTOCh chan brokersdtos.FailedTransactionDTO) error
	SuccessTransactionReader(ctx context.Context, successWalletTransferDto chan brokersdtos.SuccessTransactionDTO) error
	ApprovedTransactionReader(ctx context.Context, approvedWalletTransferDto chan brokersdtos.ApprovedTransactionDTO) error
}
