package brokers

import (
	"discount/internal/brokers/brokersdtos"
	"encoding/json"
	"fmt"

	amqp "github.com/rabbitmq/amqp091-go"
)

func (r *RabbitMQ) PublishPendingTransaction(amount float64, mobile string) error {
	conn, err := amqp.Dial(r.Host)
	if err != nil {
		return fmt.Errorf("publisher >> Enqueue >> %w", err)
	}

	defer conn.Close()

	channel, err := conn.Channel()
	if err != nil {
		return fmt.Errorf("publisher >> Enqueue >> %w", err)
	}

	defer channel.Close()

	pendingTransactionDTO := brokersdtos.PendingTransactionDTO{
		Amount: amount,
		Mobile: mobile,
	}
	payload, err := json.Marshal(pendingTransactionDTO)
	if err != nil {
		return fmt.Errorf("publisher >> Enqueue >> %w", err)
	}

	if err = channel.Publish(
		r.Exchange,
		r.PendingTransactionRoutingKey,
		false,
		false,
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "application/json",
			ContentEncoding: "",
			Body:            payload,
			DeliveryMode:    amqp.Transient,
			Priority:        0,
		},
	); err != nil {
		return fmt.Errorf("publisher >> Enqueue >> %w", err)
	}

	return nil
}
