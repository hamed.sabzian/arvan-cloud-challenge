package brokersdtos

type SuccessTransactionDTO struct {
	Amount float64 `json:"amount"`
	Mobile string  `json:"mobile"`
}
