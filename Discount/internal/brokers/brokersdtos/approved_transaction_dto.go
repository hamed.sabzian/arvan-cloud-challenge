package brokersdtos

type ApprovedTransactionDTO struct {
	Amount float64 `json:"amount"`
	Mobile string  `json:"mobile"`
}
