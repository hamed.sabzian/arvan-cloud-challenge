package brokersdtos

type FailedTransactionDTO struct {
	Amount string `json:"amount"`
	Mobile string `json:"mobile"`
}
