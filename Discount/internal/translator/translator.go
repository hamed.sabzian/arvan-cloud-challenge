package translator

const (
	EN = "en"
	FA = "fa"
)

func Translate(message string, lang string) string {
	if lang == EN {
		return englishMessages[message]
	}
	if lang == FA {
		return persianMessages[message]
	}

	return "" //TODO: add error
}
