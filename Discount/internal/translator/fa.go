package translator

var persianMessages map[string]string = map[string]string{
	"this.field.is.required": "این فیلد ضروری می باشد.",
	"validation.error":       "اطلاعات ارسال شده صحیح نمی باشد",
	"not.found.entity":       "موجودیت یافت نشد.",
}
