package translator

var englishMessages map[string]string = map[string]string{
	"this.field.is.required": "This field is required",
	"validation.error":       "Validation Error",
	"not.found.entity":       "Not found entity",
}
