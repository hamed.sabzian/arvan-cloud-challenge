package services

import (
	"discount/internal/brokers/brokersdtos"
	"discount/internal/handlers/dtos"
	"discount/internal/handlers/mappers"
	"discount/internal/models"
	"discount/internal/validation"

	"errors"
	"fmt"
)

func (s Service) ApplyDiscount(applyDisountDTO dtos.ApplyDisountDTO) error {
	var err error

	err = s.Validator.Validate(applyDisountDTO)
	if err != nil {
		var customValidationError *validation.CustomValidationError
		if errors.As(err, &customValidationError) {
			return mappers.CustomValidationErrorToValidationErrorDTO(*customValidationError)
		}
		return err
	}

	tx := s.Repository.Begin()

	defer func() {
		if err != nil {
			_ = tx.Rollback()
		}
	}()

	discount, err := s.Repository.GetDiscountByCode(tx, applyDisountDTO.Code)
	if err != nil {
		var notFound *models.ErrorNotFound
		if !errors.As(err, &notFound) {
			return fmt.Errorf("FailedTransaction >> %w", err)
		}
	}

	// hasTransactionForMobile := discount.HasTransactionForMobile(applyDisountDTO.Mobile)
	// if hasTransactionForMobile == true{
	// 	return err
	// }

	transaction := models.Transaction{
		DiscountID: discount.ID,
		Status:     models.PendingTransactionStatus,
	}
	transaction.Mobile = applyDisountDTO.Mobile

	newTransaction, err := discount.AddNewTransaction(transaction)
	if err != nil {
		if errors.Is(err, models.DiscountFullFillError) {
			return err
		}
		if errors.Is(err, models.UserUsedDiscount) {
			return err
		}
		return err
	}

	discount, err = s.Repository.UpdateDiscount(tx, *discount)
	if err != nil {
		return err
	}

	_, err = s.Repository.AddTransaction(tx, *newTransaction)
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return nil
	}

	err = s.Broker.PublishPendingTransaction(10000000, applyDisountDTO.Mobile)
	if err != nil {
		return err
	}

	return nil
}

func (s Service) SuccessTransaction(successTransactionDTO brokersdtos.SuccessTransactionDTO) error {

	tx := s.Repository.Begin()
	var err error

	defer func() {
		if err != nil {
			_ = tx.Rollback()
		}
	}()

	discountTransaction, err := s.Repository.GetPendingDiscountTransaction(tx, successTransactionDTO.Mobile)
	if err != nil {
		var notFound *models.ErrorNotFound
		if !errors.As(err, &notFound) {
			return fmt.Errorf("FailedTransaction >> %w", err)
		}
	}

	discount, err := s.Repository.GetDiscountByID(tx, discountTransaction.DiscountID)
	if err != nil {
		var notFound *models.ErrorNotFound
		if !errors.As(err, &notFound) {
			return fmt.Errorf("FailedTransaction >> %w", err)
		}
	}

	transaction, err := discount.SetTransactionSuccessStatus(discountTransaction.ID)
	if err != nil {
		return err
	}

	_, err = s.Repository.UpdateDiscountTransaction(tx, *transaction)
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return nil
	}

	err = s.Broker.PublishApprovedTransaction(1000000, transaction.Mobile)
	if err != nil {
		return err
	}

	return nil
}

func (s Service) FailedTransaction(failedTransactionDTO brokersdtos.FailedTransactionDTO) error {

	tx := s.Repository.Begin()
	var err error

	defer func() {
		if err != nil {
			_ = tx.Rollback()
		}
	}()

	discountTransaction, err := s.Repository.GetPendingDiscountTransaction(tx, failedTransactionDTO.Mobile)
	if err != nil {
		var notFound *models.ErrorNotFound
		if !errors.As(err, &notFound) {
			return fmt.Errorf("FailedTransaction >> %w", err)
		}
	}

	discount, err := s.Repository.GetDiscountByID(tx, discountTransaction.DiscountID)
	if err != nil {
		var notFound *models.ErrorNotFound
		if !errors.As(err, &notFound) {
			return fmt.Errorf("FailedTransaction >> %w", err)
		}
	}

	transaction, err := discount.SetTransactionFailedStatus(discountTransaction.ID)
	if err != nil {
		return err
	}

	_, err = s.Repository.UpdateDiscount(tx, *discount)
	if err != nil {
		return err
	}

	_, err = s.Repository.UpdateDiscountTransaction(tx, *transaction)
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return nil
	}

	return nil
}
