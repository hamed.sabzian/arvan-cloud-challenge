package services

import (
	"discount/internal/brokers"
	"discount/internal/brokers/brokersdtos"
	"discount/internal/configs"
	"discount/internal/handlers/dtos"
	"discount/internal/repositories"
	"discount/internal/validation"
)

type IService interface {
	ApplyDiscount(applyDisountDTO dtos.ApplyDisountDTO) error
	SuccessTransaction(successTransactionDTO brokersdtos.SuccessTransactionDTO) error
	FailedTransaction(failedTransactionDTO brokersdtos.FailedTransactionDTO) error
}

type Service struct {
	Repository repositories.IRepository
	Broker     brokers.Broker
	EnvConfig  configs.EnvConfigs
	Validator  validation.ValidationEngine
}

func NewService(repositories repositories.IRepository, broker brokers.Broker,
	validator validation.ValidationEngine, envConfig configs.EnvConfigs) Service {

	service := Service{
		Repository: repositories,
		Broker:     broker,
		EnvConfig:  envConfig,
		Validator:  validator,
	}

	return service
}
