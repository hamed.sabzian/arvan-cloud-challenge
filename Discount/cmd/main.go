package main

import (
	"discount/internal/brokers"
	"discount/internal/configs"
	"discount/internal/handlers"
	"discount/internal/repositories"
	"discount/internal/services"
	"discount/internal/validation"
)

func main() {

	envConfigs := configs.LoadEnvConfigs()

	sqlxConnection := repositories.InitSqlx(envConfigs)

	repository := repositories.NewSqlxRepository(sqlxConnection)

	validator := validation.NewValidator()

	rabbitmq := brokers.NewRabbitmq(envConfigs)

	service := services.NewService(repository, rabbitmq, *validator, envConfigs)

	eventHandlerEngine := handlers.NewEventHandler(service, rabbitmq)

	err := eventHandlerEngine.StartSuccessTransactionConsumer()
	if err != nil {
		panic(err)
	}
	err = eventHandlerEngine.StartFailedTransactionConsumer()
	if err != nil {
		panic(err)
	}
	err = eventHandlerEngine.StartApprovedTransactionConsumer()
	if err != nil {
		panic(err)
	}

	engine := handlers.SetupHttpRouter(service, envConfigs)

	engine.Run()
}
