CREATE TABLE public.discounts (
	id varchar(26) NOT NULL,
	code varchar(26) NOT NULL,
	description text NOT NULL,
	max_usage_count INT NOT NULL,
	used_count INT NOT NULL,
	created_at TIMESTAMP(6) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
	updated_at TIMESTAMP(6) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,

	CONSTRAINT pk_discounts PRIMARY KEY (id)
);