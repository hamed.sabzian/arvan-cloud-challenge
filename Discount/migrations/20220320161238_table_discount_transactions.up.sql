CREATE TABLE public.discount_transactions (
	id varchar(26) NOT NULL,
	discount_id varchar(26) NOT NULL,
	mobile varchar(26) NOT NULL,
	status varchar(26) NOT NULL,
	track_id varchar(26) NOT NULL,
	created_at TIMESTAMP(6) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,

	CONSTRAINT pk_discount_transactions PRIMARY KEY (id),
    CONSTRAINT fk_discount_id FOREIGN KEY (discount_id) REFERENCES public.discounts(id)
);