package repositories

import (
	"wallet/internal/models"

	"github.com/jmoiron/sqlx"
)

type IRepository interface {
	Begin() *sqlx.Tx
	GetWalletByMobile(tx *sqlx.Tx, mobile string) (*models.Wallet, error)
	UpdateWallet(tx *sqlx.Tx, wallet models.Wallet) (*models.Wallet, error)
	InsertTransaction(tx *sqlx.Tx, transaction models.Transaction) (*models.Transaction, error)
}
