package repositories

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
)

func (repo SqlxRepository) Begin() *sqlx.Tx {
	return repo.DB.MustBeginTx(context.Background(), &sql.TxOptions{Isolation: sql.LevelRepeatableRead})
}
