package repositories

import (
	"database/sql"
	"time"
	"wallet/internal/models"

	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"
)

type WalletDTO struct {
	ID        sql.NullString  `db:"id"`
	Mobile    sql.NullString  `db:"mobile"`
	Balance   decimal.Decimal `db:"balance"`
	CreatedAt time.Time       `db:"created_at"`
	UpdatedAt time.Time       `db:"updated_at"`
}

func (repo SqlxRepository) GetWalletByMobile(tx *sqlx.Tx, mobile string) (*models.Wallet, error) {
	var walletDTO WalletDTO

	err := tx.Get(&walletDTO, "SELECT * FROM wallet WHERE mobile = $1", mobile)
	if err != nil {
		return nil, err
	}

	wallet := models.Wallet{
		ID:     walletDTO.ID.String,
		Mobile: walletDTO.Mobile.String,
		Balance: models.Money{
			Amount: walletDTO.Balance,
		},
		CreatedAt: walletDTO.CreatedAt,
		UpdatedAt: walletDTO.UpdatedAt,
	}

	return &wallet, nil
}

func (repo SqlxRepository) UpdateWallet(tx *sqlx.Tx, wallet models.Wallet) (*models.Wallet, error) {
	wallet.UpdatedAt = time.Now()

	_, err := tx.Exec("UPDATE wallet SET balance=$1, updated_at=$2 WHERE mobile=$3", wallet.Balance, wallet.UpdatedAt, wallet.Mobile)
	if err != nil {
		return nil, err
	}

	return &wallet, nil
}
