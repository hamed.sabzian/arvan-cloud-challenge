package repositories

import (
	"wallet/internal/models"

	"github.com/jmoiron/sqlx"
)

func (repo SqlxRepository) InsertTransaction(tx *sqlx.Tx, transaction models.Transaction) (*models.Transaction, error) {

	_, err := tx.Exec("INSERT INTO transaction(amount, wallet_id, created_at, updated_at) VALUES($1, $2, $3, $4)",
		transaction.Amount, transaction.WalletID, transaction.CreatedAt, transaction.UpdatedAt)
	if err != nil {
		return nil, err
	}

	return &transaction, nil
}
