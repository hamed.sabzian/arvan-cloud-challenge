package brokers

import (
	"encoding/json"
	"fmt"
	"time"
	"wallet/internal/brokers/brokersdtos"
)

func (r *RabbitMQ) TransactionReader(increaseBalanceRequestDTO chan brokersdtos.IncreaseBalance) error {
	var increaseBalance brokersdtos.IncreaseBalance

	for { // nolint
		msgs, err := r.Channel.Consume(
			r.PendingTransactionQueue, // queue
			"",                        // consumer
			false,                     // auto ack
			false,                     // exclusive
			false,                     // no local
			false,                     // no wait
			nil,                       // args
		)
		if err != nil {
			return fmt.Errorf("broker > rabbitmq > PaymentReader > consume > %w", err)
		}

		for msg := range msgs {
			unmarshalError := json.Unmarshal(msg.Body, &increaseBalance)
			if unmarshalError != nil {
				_ = msg.Nack(true, true)
			}

			increaseBalanceRequestDTO <- increaseBalance

			msg.Ack(false)
		}

		if r.Channel.IsClosed() {
			time.Sleep(time.Duration(r.ReconnectDelay) * time.Second)
			r.Channel = r.Connect(r.Host)
		}
	}
}
