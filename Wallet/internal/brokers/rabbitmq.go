package brokers

import (
	"fmt"
	"log"
	"strconv"
	"wallet/internal/configs"

	amqp "github.com/rabbitmq/amqp091-go"
)

type RabbitMQ struct {
	Host                         string
	ReconnectDelay               int
	Exchange                     string
	ExchangeType                 string
	SuccessTransactionRoutingKey string
	FailedTransactionRoutingKey  string
	PendingTransactionQueue      string
	PendingTransactionRoutingKey string
	Channel                      *amqp.Channel
}

func (r *RabbitMQ) Connect(rabbitHost string) *amqp.Channel {
	if r.Channel != nil && !r.Channel.IsClosed() {
		return r.Channel
	}

	conn, err := amqp.Dial(rabbitHost)
	if err != nil {
		panic("err")
	}

	channel, err := conn.Channel()
	if err != nil {
		panic(err)
	}

	return channel
}

func NewRabbitmq(config configs.EnvConfigs) Broker {
	r := RabbitMQ{
		Host:                         config.RabbitMqHost,
		Exchange:                     config.Exchange,
		ExchangeType:                 config.ExchangeType,
		SuccessTransactionRoutingKey: config.SuccessTransactionRoutingKey,
		FailedTransactionRoutingKey:  config.FailedTransactionRoutingKey,
		PendingTransactionQueue:      config.PendingTransactionQueue,
		PendingTransactionRoutingKey: config.PendingTransactionRoutingKey,
	}

	var err error

	r.ReconnectDelay, err = strconv.Atoi("5")
	if err != nil {
		panic(err)
	}

	ch := r.Connect(config.RabbitMqHost)
	r.Channel = ch

	err = r.declareQueues()
	if err != nil {
		log.Fatal("Broket >> initialize >> NewRabbitmq >> Declare Queues", err)
	}

	return &r
}

func (r *RabbitMQ) declareQueues() error {
	err := r.Channel.ExchangeDeclare(
		r.Exchange,
		"direct",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("declareQueues >> ExchangeDeclare >>  %w", err)
	}

	q, err := r.Channel.QueueDeclare(
		r.PendingTransactionQueue,
		false,
		false,
		true,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("declareQueues >> QueueDeclare >> %w", err)
	}

	err = r.Channel.QueueBind(
		q.Name,
		r.PendingTransactionRoutingKey,
		r.Exchange,
		false,
		nil)

	if err != nil {
		return fmt.Errorf("declareQueues >> QueueBind >> %w", err)
	}

	return nil
}
