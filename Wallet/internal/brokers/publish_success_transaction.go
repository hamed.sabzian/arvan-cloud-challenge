package brokers

import (
	"encoding/json"
	"fmt"
	"wallet/internal/brokers/brokersdtos"

	amqp "github.com/rabbitmq/amqp091-go"
)

func (r *RabbitMQ) PublishSuccessTransaction(amount float64, mobile string) error {
	conn, err := amqp.Dial(r.Host)
	if err != nil {
		return fmt.Errorf("publisher >> Enqueue >> %w", err)
	}

	defer conn.Close()

	channel, err := conn.Channel()
	if err != nil {
		return fmt.Errorf("publisher >> Enqueue >> %w", err)
	}

	defer channel.Close()

	successTransfer := brokersdtos.SuccessWalletTransferDto{
		Amount: amount,
		Mobile: mobile,
	}

	payload, err := json.Marshal(successTransfer)
	if err != nil {
		return fmt.Errorf("publisher >> Enqueue >> %w", err)
	}

	if err = channel.Publish(
		r.Exchange,
		r.SuccessTransactionRoutingKey,
		false,
		false,
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "application/json",
			ContentEncoding: "",
			Body:            payload,
			DeliveryMode:    amqp.Transient,
			Priority:        0,
		},
	); err != nil {
		return fmt.Errorf("publisher >> Enqueue >> %w", err)
	}

	return nil
}
