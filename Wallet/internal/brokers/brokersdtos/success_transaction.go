package brokersdtos

type SuccessWalletTransferDto struct {
	Amount float64 `json:"amount"`
	Mobile string  `json:"mobile"`
}
