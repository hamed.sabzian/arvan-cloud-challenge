package brokersdtos

type FailedWalletTransferDto struct {
	Amount string `json:"amount"`
	Mobile string `json:"mobile"`
}
