package brokersdtos

type IncreaseBalance struct {
	Amount float64 `json:"amount"`
	Mobile string  `json:"mobile"`
}
