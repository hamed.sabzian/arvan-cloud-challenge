package brokers

import (
	"wallet/internal/brokers/brokersdtos"
)

type Broker interface {
	PublishFailedTransaction(amount float64, mobile string) error
	PublishSuccessTransaction(amount float64, mobile string) error
	TransactionReader(increaseBalanceRequestDTO chan brokersdtos.IncreaseBalance) error
}
