package models

import (
	"errors"
	"fmt"
)

var ErrorDuplicateEntity = errors.New("duplicate entity")

type ErrorNotFound struct {
	resource string
	ID       string
}

func (e *ErrorNotFound) Error() string {
	return fmt.Sprintf("resource: %s id: %s", e.resource, e.ID)
}

func NewErrorNotFound(resource, id string) *ErrorNotFound {
	return &ErrorNotFound{
		resource: resource,
		ID:       id,
	}
}
