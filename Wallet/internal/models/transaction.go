package models

import (
	"time"

	"github.com/shopspring/decimal"
)

type Transaction struct {
	ID        string          `db:"id"`
	WalletID  string          `db:"wallet_id"`
	Amount    decimal.Decimal `db:"amount"`
	Type      int64           `db:"type"` //TODO
	CreatedAt time.Time       `db:"created_at"`
	UpdatedAt time.Time       `db:"updated_at"`
}
