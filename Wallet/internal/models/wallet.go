package models

import (
	"time"

	"github.com/shopspring/decimal"
)

type Wallet struct {
	ID        string
	Mobile    string
	Balance   Money
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (w *Wallet) IncreaseBalance(amount decimal.Decimal) {
	w.Balance = w.Balance.Add(amount)
}

type Money struct {
	Amount   decimal.Decimal
	Currency Currency
}

type Currency struct {
}

func (m Money) New(amount float64) {
	m.Amount = decimal.NewFromFloat(amount)
}

func (m Money) Add(amount decimal.Decimal) Money {
	return Money{
		Amount:   amount,
		Currency: Currency{},
	}
}
