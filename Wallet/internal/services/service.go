package services

import (
	"wallet/internal/brokers"
	"wallet/internal/brokers/brokersdtos"
	"wallet/internal/configs"
	"wallet/internal/repositories"
)

type IService interface {
	IncreaseBalance(increaseBalanceRequest brokersdtos.IncreaseBalance) error
}

type Service struct {
	Repository repositories.IRepository
	Broker     brokers.Broker
	EnvConfig  configs.EnvConfigs
}

func NewService(repositories repositories.IRepository, broker brokers.Broker, envConfig configs.EnvConfigs) Service {

	service := Service{
		Repository: repositories,
		Broker:     broker,
		EnvConfig:  envConfig,
	}

	return service
}
