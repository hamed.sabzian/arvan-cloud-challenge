package services

import (
	"errors"
	"fmt"
	"wallet/internal/brokers/brokersdtos"
	"wallet/internal/models"

	"github.com/shopspring/decimal"
)

func (s Service) IncreaseBalance(increaseBalance brokersdtos.IncreaseBalance) error {
	tx := s.Repository.Begin()
	var err error

	defer func() {
		if err != nil {
			_ = tx.Rollback()
		}
	}()

	wallet, err := s.Repository.GetWalletByMobile(tx, increaseBalance.Mobile)
	if err != nil {
		err = s.Broker.PublishFailedTransaction(increaseBalance.Amount, increaseBalance.Mobile)
		if err != nil {
			return err
		}

		var notFound *models.ErrorNotFound
		if !errors.As(err, &notFound) {
			return fmt.Errorf("FailedTransaction >> %w", err)
		}
	}

	amount := decimal.NewFromFloat(increaseBalance.Amount)
	wallet.IncreaseBalance(amount)

	_, err = s.Repository.UpdateWallet(tx, *wallet)
	if err != nil {
		return err
	}

	newTransaction := models.Transaction{
		WalletID: wallet.ID,
		Amount:   amount,
	}

	_, err = s.Repository.InsertTransaction(tx, newTransaction)
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	err = s.Broker.PublishSuccessTransaction(increaseBalance.Amount, increaseBalance.Mobile)
	if err != nil {
		return err
	}

	return nil
}
