package handlers

import (
	"fmt"
	"wallet/internal/brokers"
	"wallet/internal/brokers/brokersdtos"
	"wallet/internal/services"
)

type EventRouter struct {
	Service                   services.IService
	Broker                    brokers.Broker
	IncreaseBalanceRequestDTO chan brokersdtos.IncreaseBalance
}

func NewEventHandler(service services.IService, broker brokers.Broker) EventRouter {
	return EventRouter{
		Service:                   service,
		Broker:                    broker,
		IncreaseBalanceRequestDTO: make(chan brokersdtos.IncreaseBalance),
	}
}

func (e *EventRouter) StartIncreaseBalanceConsumer() {
	go e.IncreaseBalanceReader()
	e.Broker.TransactionReader(e.IncreaseBalanceRequestDTO)
}

//nolint: forbidigo
func (e EventRouter) IncreaseBalanceReader() {
	for increaseBalanceRequest := range e.IncreaseBalanceRequestDTO {
		err := e.Service.IncreaseBalance(increaseBalanceRequest)
		if err != nil {
			fmt.Println(err)
		}
	}
}
