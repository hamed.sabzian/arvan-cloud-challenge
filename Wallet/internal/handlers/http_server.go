package handlers

import (
	"net/http"
	"wallet/internal/configs"
	"wallet/internal/services"

	"github.com/gin-gonic/gin"
)

type HttpRouter struct {
	Service    services.IService
	EnvConfigs configs.EnvConfigs
}

type IHttpRouter interface {
	Create(ctx *gin.Context)
}

func SetupHttpRouter(service services.IService, envConfigs configs.EnvConfigs) *gin.Engine {
	engine := gin.Default()

	// linkCtrl := newHttpRouter(service, envConfigs)
	// engine.GET("/wallets/:id", linkCtrl.GetWalletByID)
	return engine
}

func newHttpRouter(service services.IService, envConfigs configs.EnvConfigs) HttpRouter {
	ctrl := HttpRouter{
		Service:    service,
		EnvConfigs: envConfigs,
	}

	return ctrl
}

func (c HttpRouter) Response(ctx *gin.Context, statusCode int, responseBody interface{}) {

	if statusCode == http.StatusInternalServerError && c.EnvConfigs.EnvironmentMode != "Development" {
		ctx.JSON(statusCode, gin.H{"error": "internal error"})
	} else {
		ctx.JSON(statusCode, responseBody)
	}
}
