package dtos

import "time"

type GetWalletResponse struct {
	ID        string    `json:"id"`
	Mobile    string    `json:"mobile"`
	Balance   float64   `json:"balance"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
