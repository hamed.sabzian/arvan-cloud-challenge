CREATE TABLE public.wallet_transactions (
	id varchar(26) NOT NULL,
	wallet_id varchar(26) NOT NULL,
	amount decimal(15,2) NOT NULL,
    type varchar(26) NOT NULL,
	created_at TIMESTAMP(6) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,

	CONSTRAINT pk_wallet_transactions PRIMARY KEY (id),
    CONSTRAINT fk_wallet_id FOREIGN KEY (wallet_id) REFERENCES public.wallet_transactions(id)
);