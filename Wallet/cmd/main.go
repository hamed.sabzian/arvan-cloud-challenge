package main

import (
	"wallet/internal/brokers"
	"wallet/internal/configs"
	"wallet/internal/handlers"
	"wallet/internal/repositories"
	"wallet/internal/services"
)

func main() {

	envConfigs := configs.LoadEnvConfigs()

	sqlxConnection := repositories.InitSqlx(envConfigs)

	repository := repositories.NewSqlxRepository(sqlxConnection)

	rabbitmq := brokers.NewRabbitmq(envConfigs)

	service := services.NewService(repository, rabbitmq, envConfigs)

	eventHandlerEngine := handlers.NewEventHandler(service, rabbitmq)

	eventHandlerEngine.StartIncreaseBalanceConsumer()

	engine := handlers.SetupHttpRouter(service, envConfigs)

	engine.Run()
}
